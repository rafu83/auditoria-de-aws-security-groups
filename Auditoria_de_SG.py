# Descripcion       :Este script monitorea cambios en las politicas de los Security Groups en AWS
# Autor             :Andres Fuentes Martinez
# Fecha             :15 junio 2021
# Version           :1.0
# Notas             :refierase a la documentacion para el uso del programa
# Version de python :3.8

import json
import boto3
import re

# Access Security Groups API
client = boto3.client('ec2')

def lambda_handler(event, context):
    
    ## Open the referense Security Groups file
    JsonDataFile_ref = open("ref_policy.json")
    JsonData_ref = json.load(JsonDataFile_ref)

    ## Get all Security Groups that are in production from AWS
    JsonData_aws = client.describe_security_groups()

    ref = JsonData_ref["SecurityGroups"]
    aws = JsonData_aws["SecurityGroups"]


    ###
    def compare():

        dict_keys_SecurityGroups = ref[0].keys()
        keys_SecurityGroups = list(dict_keys_SecurityGroups)

        len_ref = len(ref)
        len_aws = len(aws)

        ## Start comparison
        for refCount in range(len_ref):
    
            for awsCount in range(len_aws):

                # Compare the two objects with the same GroupName.
                if ref[refCount]["GroupName"] == aws[awsCount]["GroupName"]:

                    # If the GroupNames are different start comparing the keys inside the object.
                    if ref[refCount] != aws[awsCount]:
                        print()
                        print("WARNING: Policy differs in: '{}'".format(ref[refCount]["GroupName"]))

                        # Extract all the keys from the object (not nested)
                        for KeySG in keys_SecurityGroups:

                            if ref[refCount][KeySG] != aws[awsCount][KeySG]:

                                ## Separate if the type is 'str' or 'list' and treat them differently
                                # Show 'str' only
                                if type(ref[refCount][KeySG]) == str:
                                    print("WARNING: OLD {}: {}".format(KeySG,ref[refCount][KeySG]))
                                    print("WARNING: NEW {}: {}".format(KeySG,aws[awsCount][KeySG]))
                                    print()
                                # Get inside the first nest and extract all the keys
                                # (Problema por solucionar!!)
                                else: 
                                    if len(ref[refCount][KeySG]) == 0:
                                        print("WARNING: OLD {}: {}".format(KeySG,ref[refCount][KeySG]))

                                        ###### TESTING

                                        if len(aws[awsCount][KeySG]) > 0:

                                            keys_lv1 = list(aws[awsCount][KeySG][0].keys())

                                            for t in keys_lv1:
                                                print("WARNING: NEW {} > {}: {}".format(KeySG,t,aws[awsCount][KeySG][0][t]))
                                        
                                        ###### TESTING
                              
                                    else:
                                        if len(ref[refCount][KeySG]) > 0:
                                        # Get the keys inside the nest
                                            keys_lv0 = list(ref[refCount][KeySG][0].keys())

                                            for i in keys_lv0:
                                        
                                                if ref[refCount][KeySG][0][i] != aws[awsCount][KeySG][0][i]:
                                            
                                                    print("WARNING: OLD {} > {}: {}".format(KeySG,i,ref[refCount][KeySG][0][i]))
                                                    print("WARNING: NEW {} > {}: {}".format(KeySG,i,aws[awsCount][KeySG][0][i]))

    
    ## Check if both policies are equal, exit if true
    if ref == aws:
        print("Ok")
    # If the policies are not equal check why
    else:
        # Check if policies has been added
        if len_ref < len_aws:
            print("WARNING: {} new policy added.".format(len_aws - len_ref))
            compare()
        # Check if policies have been removed
        elif len_ref > len_aws:
            print("WARNING: {} policy removed.".format(len_ref - len_aws))
            compare()
        # If both policies have the same amount of json-obects
        elif len_ref == len_aws:
            compare()
        # If errors are found but can't specify them
        else:
            print("WARNING: Anomaly found, please check manually.")